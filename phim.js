async function listMovie(url, pages) {
  const response = await fetch(url + pages);
  const ls_Movie = await response.json();
  console.log(ls_Movie);
  var path = "https://image.tmdb.org/t/p/w200";
  $("#itemMovie").empty();
  $(".pagination").empty();
  for (const item of ls_Movie.results) {
    $("#itemMovie").append(`
            <div class="col-md-3 hei_item" >
              <div class="card mb-4 shadow-sm">
                <p class = "tag">${"Rate: " + item.vote_average}</p>
                <img class="img_hei" src="${path + item.poster_path}"/>
                <div class="card-body">
                  <p class="card-text">${item.name}</p>
                  <p class="date_item">${item.first_air_date}</p>
                  <button class="btn_item" onclick = "detailMovie(${
                    item.id
                  })">Chi tiết</button>
                </div>
              </div>
            </div>
            
      `);
  }
}

async function listMovie1(url) {
  const response = await fetch(url);
  const ls_Movie = await response.json();
  console.log(ls_Movie);
  var path = "https://image.tmdb.org/t/p/w200";
  $("#itemMovie").empty();
  for (const item of ls_Movie.results) {
    $("#itemMovie").append(`
            <div class="col-md-3 hei_item" >
              <div class="card mb-4 shadow-sm">
                <p class = "tag">${"Rate: " + item.vote_average}</p>
                <img class="img_hei" src="${path + item.poster_path}"/>
                <div class="card-body">
                  <p class="card-text">${item.name}</p>
                  <p class="date_item">${item.first_air_date}</p>
                  <button class="btn_item" onclick = "detailMovie(${
                    item.id
                  })">Chi tiết</button>
                </div>
              </div>
            </div>
            
      `);
  }
}

async function pagin(url, page) {
  const response = await fetch(url);
  const load = await response.json();
  if (load.total_pages == undefined) page = 0;
  else if (load.total_pages <= 5) {
    page = load.total_pages;
  } else if (load.total_pages > 5) page = 5;
  if (page > 0) {
    for (let i = 1; i <= page; i++) {
      $("#itemMovie").empty();
      $(".pagination").append(`
      <li><button id = "abc" onclick="listMovie1(\`${url}${i}\`)">${i}</button></li>
      `);
    }
  }
}

async function searchMovieName(event) {
  event.preventDefault();
  const name_Movie = $("#search_movie").val();
  console.log(name_Movie);
  const url = `https://api.themoviedb.org/3/search/tv?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US&query=${name_Movie}&page=`;
  pages = 1;
  $("#itemMovie").empty();
  $(".pagination").empty();

  $("#itemMovie").append(
    `<div class="lds-facebook"><div></div><div></div><div></div></div>`
  );
  listMovie(url, pages);
}

async function searchMovieActor(id) {
  event.preventDefault();
  $(".pagination").empty();
  const url = `https://api.themoviedb.org/3/person/${id}?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US
  `;
  DetailActor(url);
}

async function DetailActor(url) {
  const response = await fetch(url);
  const detail_actor = await response.json();
  var temp = "https://image.tmdb.org/t/p/w500";
  console.log(detail_actor);
  $("#itemMovie").empty();
  $(".pagination").empty();
  $("#itemMovie").append(
    `<div class="lds-facebook"><div></div><div></div><div></div></div>`
  );
  $("#itemMovie").empty();
  var nameElse = [];
  for (const item of detail_actor.also_known_as) {
    nameElse.push(item.name);
  }
  $("#itemMovie").append(`
  <div class="col-md-6">
    <img class="img-fluid content_detail" src="${temp +
      detail_actor.profile_path}"/>
  </div>

  <div class="col-md-6">
    <h2 class="my-3 tit">${detail_actor.name}</h2>
    <dl>
          <span class="dt">Aslo known as :</span>
          <span>${detail_actor.nameElse}</span>
      </dl>
      <dl>
          <span class="dt">Biography:</span>
          <span>${detail_actor.biography}</span>
      </dl>
      <dl>
          <span class="dt">Birthday :</span>
          <span>${detail_actor.birthday}</span>
      </dl>
      <dl>
          <span class="dt">Deathday :</span>
          <span>${detail_actor.deathday}</span>
      </dl>
  </div>
  <h3 class="my-4 tit content_detail">List Film</h3>
  </div>
  <div class="row ls_actor" id = "infoMovie"></div> 
  `);

  infoMovie(detail_actor.id);
}

async function infoMovie(id) {
  url = `https://api.themoviedb.org/3/person/${id}/tv_credits?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US
  `;
  const response = await fetch(url);
  const infor_Movie = await response.json();
  $(".pagination").empty();
  var path = "https://image.tmdb.org/t/p/w300";
  for (const item of infor_Movie.cast) {
    $("#infoMovie").append(`
      <div class="col-md-3 col-sm-6 mb-4 actor">
        <a href="#" onclick ="detailMovie(${item.id})">
          <img class="img-fluid" src="${path + item.poster_path}"/>
        </a>
        <div>
          <p class="dt">${item.name}</p>
      </div>
    `);
  }
}

async function detailMovie(id) {
  url = `https://api.themoviedb.org/3/tv/${id}?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US`;
  $("#itemMovie").empty();
  $(".pagination").empty();
  $("#itemMovie").append(
    `<div class="lds-facebook"><div></div><div></div><div></div></div>`
  );
  $("#itemMovie").empty();

  var temp = "https://image.tmdb.org/t/p/w500";
  const response = await fetch(url);
  const detail_movie = await response.json();
  var gen = [];
  var namePro = [];
  for (const item of detail_movie.genres) {
    gen.push(item.name);
  }
  for (const item of detail_movie.production_companies) {
    namePro.push(item.name);
  }
  $("#itemMovie").append(`
  <div class="col-md-6">
    <img class="img-fluid content_detail" src="${temp +
      detail_movie.poster_path}"/>
  </div>

  <div class="col-md-6">
    <h2 class="my-3 tit">${detail_movie.name}</h2>
    <div class="content">
      <dl>
          <span class="dt">Overview:</span>
          <span>${detail_movie.overview}</span>
      </dl>
      <dl>
          <span class="dt">Release:</span>
          <span>${detail_movie.first_air_date}</span>
      </dl>
      <dl>
          <span class="dt">Genres:</span>
          <span>${gen}</span>
      </dl>
      <dl>
          <span class="dt">Vote:</span>
          <span>${detail_movie.vote_count}</span>
      </dl>
      <dl>
          <span class="dt">Status:</span>
          <span>${detail_movie.status}</span>
      </dl>
      <dl>
          <span class="dt">Production:</span>
          <span>${namePro}</span>
      </dl>
    </div>
  </div>
  <h3 class="my-4 tit content_detail">Casting</h3>
  </div>
  <div class="row ls_actor" id = "info"></div> 
  <h3 class="my-4 tit content_detail">Review</h3>
  <div class="row ls_actor" id = "review"></div> 
  
  `);

  infoCast(id);
  reviewMovie(id);
}

async function infoCast(id) {
  urlCast = `https://api.themoviedb.org/3/tv/${id}/credits?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US`;
  const response = await fetch(urlCast);
  const castMovie = await response.json();
  var path = "https://image.tmdb.org/t/p/w300";

  for (const item of castMovie.cast) {
    $("#info").append(`
      <div class="col-md-3 col-sm-6 mb-4 actor">
        <a onclick ="searchMovieActor(${item.id})">
          <img class="img-fluid" src="${path + item.profile_path}"/>
        </a>
        <div>
          <p class="dt">${item.name}</p>
          <p>${item.character}</p>
      </div>
    `);
  }
}

async function reviewMovie(id) {
  url = `https://api.themoviedb.org/3/tv/${id}/reviews?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US&page=1`;
  const response = await fetch(url);
  const review_movie = await response.json();

  for (const item of review_movie.results) {
    $("#review").append(`
        <div class="review">
        <dl>
          <span class="dt">Author:</span>
          <span>${item.author}</span>
        </dl>        
        <dl>
          <span class="dt">Content:</span>
          <span>${item.content}</span>
        </dl>
      </div>
    `);
  }
}
pagin(
  "https://api.themoviedb.org/3/tv/top_rated?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US&page=",
  "1"
);
listMovie1(
  "https://api.themoviedb.org/3/tv/top_rated?api_key=27f29be409a17e0f2a6dc7ef87f5e424&language=en-US&page=1"
);
